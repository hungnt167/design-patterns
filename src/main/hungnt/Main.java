package main.hungnt;

import factory.hungnt.Shape;
import factory.hungnt.ShapeFactory;

/**
 * Created by Nguyen on 5/27/2016.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("=========== Begin Factory pattern ============");
        ShapeFactory shapeFactory = new ShapeFactory();
        Shape rect = shapeFactory.getShape("Rectangle");
        Shape circle = shapeFactory.getShape("Circle");
        Shape square = shapeFactory.getShape("Square");
        rect.draw();
        circle.draw();
        square.draw();
        System.out.println("=========== End Factory pattern ============");
    }
}
