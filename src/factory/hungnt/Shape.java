package factory.hungnt;

/**
 * Created by Nguyen on 5/27/2016.
 */
public interface Shape {
    public void draw();
}
