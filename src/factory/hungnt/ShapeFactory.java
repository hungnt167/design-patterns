package factory.hungnt;

/**
 * Created by Nguyen on 5/27/2016.
 */
public class ShapeFactory {
    public Shape getShape(String shapeType){
        if(shapeType.equalsIgnoreCase("Rectangle")){
            return  new Rectangle();
        }
        if(shapeType.equalsIgnoreCase("Circle")){
            return  new Circle();
        }
        if(shapeType.equalsIgnoreCase("Square")){
            return  new Square();
        }
        return null;
    }
}
